import React, { Component } from 'react';
import User from './User';
import * as API from '../api'
class GetUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users:[]
          }
    }

async getUser()
{
 const users= await API.fetchUsers()
 this.setState({users})
 console.log("sam",this.state.users)
}
    componentDidMount()
    {
        this.getUser()
    }
    render() { 
        const {users}=this.state
        return ( 
            <div>
            {users.map(({name,email})=>(
                <User name={name} email={email}/>
            ))}
            </div>
         );
    }
}
 
export default GetUsers;