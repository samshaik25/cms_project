require('dotenv').config()
const {createPool}=require("mysql")

const pool=createPool({
    host:"localhost",
    user:"sam",
    password:"password",
    database:"test",
    connectionLimit:10
});

module.exports=pool;