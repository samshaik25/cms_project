const pool=require("../config/db")

module.exports={

    create:(data,callback)=>{
        console.log(data)
        pool.query(`INSERT into User (name,email,password) values(?,?,?)`,
        [
            data.name,data.email,data.password
        ],
        (err,results)=>{
           if(err){
                return callback(err)
           } 

             return  callback(null,results)
           
        }
        )
    },

    getUsers:callback=>{
    pool.query(`select * from User`,(err,results)=>{
        if(err){
            return callback(err)
        }
        return callback(null,results)
    })
    },
    getUserById:(id,callback)=>{
        pool.query(`select * from User where id=?`,[id],
        (err,results)=>{
            if(err){
                 return callback(err)
            }
            return callback(null,results)
        }
        )
    },
    UpdateUser:(data,callback)=>{
        console.log(data)
        pool.query(`update User set name=?,email=?,password=? where id=?`,
        [
            data.name,
            data.email,
            data.password,
            data.id
        ],
        (err,results)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results)
        }
        )
    },
    deleteUser:(data,callback)=>{
        console.log(data.id)
        pool.query(`delete from User where id=?`,[data.id],
        (err,results)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results[0])
        })
    },
    getUserByEmail:(email,callback)=>{
        console.log(email)
        pool.query(`select * from User where email=? `,[email],
        (err,results)=>{
            if(err){
              return  callback(err)
            }
            return callback(null,results[0])
        })
    },
    signup:(data,callback)=>{
        console.log(data)
        pool.query(`INSERT into User (name,email,password) values(?,?,?)`,
        [
            data.name,data.email,data.password
        ],
        (err,results)=>{
           if(err){
                return callback(err)
           } 

             return  callback(null,results)
           
        }
        )
    },
    
}