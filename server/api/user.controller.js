const { create, getUsers,getUserById,UpdateUser,deleteUser, getUserByEmail,signup}=require('./user.service')

const { genSaltSync,hashSync, compareSync}=require('bcrypt')

const {sign}= require("jsonwebtoken")

module.exports={

    createUser:(req,res)=>{

        const body=req.body;
        console.log(body)

        const salt=genSaltSync(10)
        body.password=hashSync(body.password,salt)
         create(body,(err,results)=>{
             if(err){
                 
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
             } 
            return res.send(results) 
         })

    },
    getUserById:(req,res)=>{
        const id=req.params.id;
        getUserById(id,(err,results)=>{
            if(err){
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
            }
            res.send(results)
        })
    },
    getUsers:(req,res)=>{

        getUsers((err,results)=>{
            if(err){
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
            }
            res.send(results)
        })
    },

    UpdateUser:(req,res)=>{
        const body=req.body;
        const salt=genSaltSync(10);
        body.password=hashSync(body.password,salt);
        UpdateUser(body,(err,results)=>{
            if(err){
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
            }
            if(!results){
                return res.status(404).json({
                    message:
                      err.message || " not found id"
                  });
            }
            res.send(results)
        })
    },
    deleteUser:(req,res)=>{
        const body=req.body;
        deleteUser(body,(err,results)=>{
            if(err){
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
            }
            // if(!results){
            //     return res.status(404).json({
            //         message:
            //          " not found id"
            //       });
            // }
          return  res.send({
                message:`Company deleted succesfully with id: `
              })
        })
    },
     login:(req,res)=>{
         const body=req.body;
         getUserByEmail(body.email,(err,results)=>{
             if(err){
                 console.log(err)
             }
             if(!results){
                 return res.json({
                     data:"invalid emal"
                 })
             }
             console.log(results.password)
             console.log(body.password)
            const result=compareSync(body.password,results.password)
            console.log(result)
            if(result){
                results.password=undefined;
                const jsontoken=sign({result:results},"qwerty",{
                    expiresIn:"1h"
                })
                return res.send(
                    
                jsontoken
                )
            }
            else{
                return res.json({
                    message:"invalid em"
                })
            }
         })
     }, 
     signup:(req,res)=>{

        const body=req.body;
        console.log(body)

        const salt=genSaltSync(10)
        body.password=hashSync(body.password,salt)
         signup(body,(err,results)=>{
             if(err){
                 
                return res.status(500).json({
                    message:
                      err.message || "Some error occurred while creating the Product."
                  });
             } 
            return res.send(results) 
         })

    },

}