const {createUser,getUsers, getUserById, UpdateUser, deleteUser, login,signup}=require('./user.controller');

const router=require("express").Router();
const {checkToken}=require("../authentication/token.validation")

router.post('/',checkToken,createUser);

router.get('/', getUsers)

router.get('/:id',checkToken,getUserById)

router.put('/',checkToken,UpdateUser)

router.delete('/',checkToken,deleteUser)

router.post('/login',login)

router.post("/signup",signup)

// router.get("/getInfo")
module.exports=router